package http

var TempScHtml = `
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" sizes="96x96" href="//{{.Domain}}/xlog/assets/img/favicon.png">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta http-equiv="refresh" content="60">	

	<title>{{.Title}}</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />

    <!-- Bootstrap core CSS     -->
    <link href="//{{.Domain}}/xlog/assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--  Paper Dashboard core CSS    -->
    <link href="//{{.Domain}}/xlog/assets/css/paper-dashboard.css" rel="stylesheet"/>

    <!--  Fonts and icons     -->
    <link href="//{{.Domain}}/xlog/assets/css/font-awesome.min.css" rel="stylesheet">
     <link href='//{{.Domain}}/xlog/assets/css/css.css' rel='stylesheet' type='text/css'>
	<link href="//{{.Domain}}/xlog/assets/css/themify-icons.css" rel="stylesheet">

    <!-- 引入 ECharts 文件 -->
	<script type="text/javascript" src="//{{.Domain}}/xlog/assets/js/jquery.js"></script>
    <script src="//{{.Domain}}/xlog/assets/js/echarts.min.js"></script>
</head>
<body>

<div class="wrapper">
	<div class="sidebar" data-background-color="white" data-active-color="success">

    <!--
		Tip 1: you can change the color of the sidebar's background using: data-background-color="white | black"
		Tip 2: you can change the color of the active button using the data-active-color="primary | info | success | warning | danger"
	-->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="#">
                    <img src="//{{.Domain}}/xlog/assets/img/logo.png" width="150" height="70" />
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="/">
                        <i class="ti-home"></i>
                        <p>Summary</p>
                    </a>
                </li>
                <li class="active">
                    <a href="statuscode">
                        <i class="ti-bar-chart-alt"></i>
                        <p>Status Code</p>
                    </a>
                </li>
                <li>
                    <a href="useragent">
                        <i class="ti-world"></i>
                        <p>User Agent</p>
                    </a>
                </li>
                <li>
                    <a href="remoteaddress">
                        <i class="ti-desktop"></i>
                        <p>Remote Address</p>
                    </a>
                </li>
                <li>
                    <a href="requesturi">
                        <i class="ti-files"></i>
                        <p>Request Uri</p>
                    </a>
                </li>
                <li>
                    <a href="bodybytes">
                        <i class="ti-server"></i>
                        <p>Body Bytes</p>
                    </a>
                </li>
				<li>
                    <a href="referer">
                        <i class="ti-link"></i>
                        <p>Referer</p>
                    </a>
                </li>
				<li>
                    <a href="chinamaps">
                        <i class="ti-map-alt"></i>
                        <p>China Maps</p>
                    </a>
                </li>				
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                               <i class="ti-reload"></i> Updated now
                            </div>
                            <div class="content">
								<div id="codetrenderr" style="width: 100%;height:250px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
				
				<div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                               <i class="ti-reload"></i> Updated now
                            </div>
                            <div class="content">
								<div id="codetrendok" style="width: 100%px;height:250px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
	<script type="text/javascript">
    	$(document).ready(function(){
			dashboard.getCodeTrendErr();
			dashboard.getCodeTrendok();
    	});
	</script>
	
	<script src="//{{.Domain}}/xlog/assets/js/xlog.js"></script>
</html>
`
